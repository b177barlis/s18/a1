let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ['pikachu', 'charizard', 'squirtle', 'bulbasur'],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},	
	talk: function(){
		console.log("Pikachu, I choose you!")
	}
}

console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of dot notation:');
console.log(trainer['pokemon']);
console.log('Result of talk method:');
trainer.talk();

function Pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		let damagedHealth = target.health - this.attack;

		console.log(target.name + "'s health is now reduced to " + damagedHealth + '.');
		if(damagedHealth <= 0){
			console.log(target.name + ' fainted.');
		}
	}
}


let squirtle = new Pokemon("Squirtle", 3, 4, 5);
console.log(squirtle);

let pidgey = new Pokemon("Pidgey", 16,);
console.log(pidgey);

let ekans = new Pokemon("Ekans", 32);
console.log(ekans);

let zubat = new Pokemon("Zubat", 100);
console.log(zubat);

squirtle.tackle(pidgey);
zubat.tackle(squirtle);
